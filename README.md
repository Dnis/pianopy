# pianopy
## Introduction

Pianopy is a Python wrapper for the variable selection library [piano](https://gitlab.com/Dnis/piano). 

## Installation
### Building from source
You need the requirements for piano to build pianopy.
Call `python setup.py install --user` in a root folder in order to compile piano and build pianopy.

## Examples
First we generate some synthetic regression data:

    from pianopy.data_generation import *

    # Generate regression data with 1000 samples, 20 variables and 5 true predictors
    data_gen = SyntheticRegressionData(1000, 20, 5)
    
    d = data_gen.draw()
    X = self.d["X"]
    y = self.d["y"]

### Computing a solution to the best subset selection problem via CPLEX
We can then compute an exact solution:

    from pianopy.bss.exact import mip

    # Provide X, y, and the number of used predictors
    sol = mip(X, y, 5)

### Computing a heuristic solution to the best subset selection problem via PADM
We can also get a heurstic solution much more efficiently via an penalized alternating direction approach.
Even though, the solution is not guaranteed to be globally optimal, 
it is often very close to the exact solution.

    from pianopy.bss.inexact import padm
    sol = padm(X, y, 5)