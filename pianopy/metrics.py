#  Copyright 2021 Dennis Kreber
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

def selection_accuracy(computed_solution, synthethic_regression_data):
    true_set = set(synthethic_regression_data["S"])
    computed_set = set(computed_solution["subset"])
    l = len(true_set) + len(computed_set)

    return 1 - len(true_set ^ computed_set) / l


def selection_errors(computed_solution, synthethic_regression_data):
    true_set = set(synthethic_regression_data["S"])
    computed_set = set(computed_solution["subset"])

    return len(true_set ^ computed_set) / 2.0


def relative_risk(computed_solution, synthetic_regression_data):
    beta = computed_solution["full_coefficients"]
    beta.shape = computed_solution["p"]
    beta0 = synthetic_regression_data["beta"]
    beta0.shape = beta.shape
    sigma = synthetic_regression_data["sigma"]

    return (beta - beta0).transpose().dot(sigma.dot(beta - beta0)) / beta0.transpose().dot(sigma.dot(beta0))


def relative_test_error(computed_solution, synthetic_regression_data):
    beta = computed_solution["full_coefficients"]
    beta.shape = computed_solution["p"]
    beta0 = synthetic_regression_data["beta"]
    beta0.shape = beta.shape
    sigma = synthetic_regression_data["sigma"]
    error_variance = synthetic_regression_data["error_variance"]

    return ((beta - beta0).transpose().dot(sigma).dot(beta - beta0) + error_variance) / error_variance


def proportion_of_variance_explained(computed_solution, synthetic_regression_data):
    beta = computed_solution["full_coefficients"]
    beta.shape = computed_solution["p"]
    beta0 = synthetic_regression_data["beta"]
    beta0.shape = beta.shape
    sigma = synthetic_regression_data["sigma"]
    error_variance = synthetic_regression_data["error_variance"]

    return ((beta - beta0).transpose().dot(sigma).dot(beta - beta0) + error_variance) / (
            beta0.transpose().dot(sigma).dot(beta0) + error_variance)
