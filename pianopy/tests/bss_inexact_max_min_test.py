#  Copyright 2021 Dennis Kreber
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import unittest

from pianopy.data_generation import SyntheticRegressionData
from pianopy.bss.inexact import max_min


class BssInexactMaxMinTestCase1(unittest.TestCase):
    def test_max_min_works1(self):
        data_gen = SyntheticRegressionData(1000, 20, 5)
        data_gen.set_snr(1).coef_gen.set_min_interval(1).set_max_interval(1)
        d = data_gen.draw()
        X = d["X"]
        y = d["y"]

        max_min(X, y, 5, mute=True)

    def test_max_min_works2(self):
        data_gen = SyntheticRegressionData(500, 40, 19)
        data_gen.set_seed(32868828).set_snr(1).coef_gen.set_min_interval(1).set_max_interval(1)
        data_gen.correlation_gen.set_param(0)
        d = data_gen.draw()
        X = d["X"]
        y = d["y"]

        max_min(X, y, 5)

    def test_failed_instance_now_works(self):
        data_gen = SyntheticRegressionData(18000, 8000, 2500)
        data_gen.set_seed(14942603).set_snr(1).coef_gen.set_min_interval(1).set_max_interval(1)
        data_gen.correlation_gen.set_param(0)
        d = data_gen.draw()
        X = d["X"]
        y = d["y"]

        max_min(X, y, 2500, timelimit=3600)


if __name__ == '__main__':
    unittest.main()
