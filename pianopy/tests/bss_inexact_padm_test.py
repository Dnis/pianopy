import unittest

from pianopy.data_generation import SyntheticRegressionData
from pianopy.bss.inexact import padm


class BssInexactPADMTestCase(unittest.TestCase):
    def setUp(self) -> None:
        data_gen = SyntheticRegressionData(1000, 20, 5)
        data_gen.set_snr(1).coef_gen.set_min_interval(1).set_max_interval(1)
        self.d = data_gen.draw()
        self.X = self.d["X"]
        self.y = self.d["y"]

    def test_padm_works(self):
        padm(self.X, self.y, 5)



if __name__ == '__main__':
    unittest.main()
