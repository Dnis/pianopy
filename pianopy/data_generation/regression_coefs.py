#  Copyright 2021 Dennis Kreber
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from threadpoolctl import threadpool_limits

from numpy.random import uniform
import numpy as np


class SyntheticRegressionCoefs:
    def __init__(self, size):
        self.size = size
        self.minInterval = 1.0
        self.maxInterval = 10.0
        self.seed = None

    def set_min_interval(self, m):
        self.minInterval = m
        return self

    def set_max_interval(self, m):
        self.maxInterval = m
        return self

    def set_seed(self, seed):
        self.seed = seed
        return self

    def draw(self, number_of_threads=1):
        with threadpool_limits(limits=number_of_threads, user_api='blas'):
            if self.seed is not None:
                np.random.seed(self.seed)
            ret = uniform(self.minInterval, self.maxInterval, self.size)
        return ret
