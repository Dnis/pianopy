#  Copyright 2021 Dennis Kreber
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from threadpoolctl import threadpool_limits

import numpy as np
from numpy.random import multivariate_normal
from numpy.random import normal
import random

from pianopy.data_generation.correlation_matrix import RhoCorrelationMatrix, ConstantOffDiagCorrelationMatrix
from pianopy.data_generation.regression_coefs import SyntheticRegressionCoefs


class SyntheticRegressionData:
    def __init__(self, n, p, number_of_nonzeros):
        self.n = n
        self.p = p
        self.number_of_nonzeros = number_of_nonzeros
        self.snr = 1.0
        self.coef_gen = SyntheticRegressionCoefs(number_of_nonzeros)
        self.correlation_gen = RhoCorrelationMatrix(p)
        self.seed = None

    def set_snr(self, snr):
        self.snr = snr
        return self

    def set_coef_gen(self, coef_gen):
        self.coef_gen = coef_gen
        return self

    def set_correlation_gen(self, correlation_gen):
        self.correlation_gen = correlation_gen
        return self

    def set_seed(self, seed):
        self.seed = seed
        self.coef_gen.set_seed(seed)
        return self

    def draw(self, number_of_threads=1):
        with threadpool_limits(limits=number_of_threads, user_api='blas'):
            if self.seed is not None:
                np.random.seed(self.seed)
            mean = np.zeros(self.p)
            # mean = [0] * self.p
            sigma = self.correlation_gen.generate()
            X = multivariate_normal(mean=mean, cov=sigma, size=self.n)
            random.seed(self.seed)
            S = random.sample(list(range(self.p)), self.number_of_nonzeros)
            beta_truncated = self.coef_gen.draw(number_of_threads=number_of_threads)
            beta = np.zeros(self.p)
            beta[S] = beta_truncated

            error_variance = beta.dot(sigma).dot(beta) / self.snr
            error = normal(loc=0, scale=error_variance ** 0.5, size=self.n)
            y = X.dot(beta) + error

        return dict(X=np.asfortranarray(X), sigma=sigma, S=S, beta=beta, error_variance=error_variance, error=error, y=y)


def generate_regression_data_from_dict(**kwargs):
    essential_keys = ("n", "p", "number_of_nonzeros")
    for k in essential_keys:
        if k not in kwargs:
            raise KeyError
    regression_data = SyntheticRegressionData(kwargs["n"], kwargs["p"], kwargs["number_of_nonzeros"])

    optional_keys = ("snr", "seed", "min_coef", "max_coef", "sigma_type", "sigma_param")
    key_functions = dict()
    key_functions["snr"] = regression_data.set_snr
    key_functions["seed"] = regression_data.set_seed
    key_functions["min_coef"] = regression_data.coef_gen.set_min_interval
    key_functions["max_coef"] = regression_data.coef_gen.set_max_interval
    key_functions["sigma_type"] = lambda type: regression_data.set_correlation_gen(
        RhoCorrelationMatrix(regression_data.p) if (type == "rho") else ConstantOffDiagCorrelationMatrix(
            regression_data.p))
    key_functions["sigma_param"] = regression_data.correlation_gen.set_param

    for k in optional_keys:
        if k in kwargs:
            key_functions[k](kwargs[k])
    number_of_threads = 1
    if "number_of_data_generation_threads" in kwargs:
        number_of_threads = kwargs["number_of_data_generation_threads"]
    else:
        if "number_of_threads" in kwargs:
            number_of_threads = kwargs["number_of_threads"]
    return regression_data.draw(number_of_threads=number_of_threads)
