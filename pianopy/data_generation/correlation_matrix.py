#  Copyright 2021 Dennis Kreber
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os

os.environ["OMP_NUM_THREADS"] = "1"  # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = "1"  # export OPENBLAS_NUM_THREADS=4
os.environ["MKL_NUM_THREADS"] = "1"  # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = "1"  # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = "1"  # export NUMEXPR_NUM_THREADS=6

import numpy as np
import scipy.sparse as sps


class RhoCorrelationMatrix:
    def __init__(self, p):
        self.p = p
        self.rho = 0.0
        self.sparse = False

    def set_rho(self, rho):
        self.rho = rho
        # if self.rho == 0:
        #     self.sparse = True
        # else:
        #     self.sparse = False
        return self

    def set_param(self, param):
        self.set_rho(param)
        return self

    def generate(self):
        if self.rho == 0:
            if self.sparse:
                return sps.eye(self.p, self.p)
            else:
                return np.eye(self.p, self.p)

        def fill_matrix(i, j):
            return self.rho ** abs(i - j)

        sigma = np.fromfunction(fill_matrix, (self.p, self.p))

        # sigma = np.zeros((self.p, self.p))
        # for i in range(self.p):
        #     for j in range(self.p):
        #         sigma[i, j] = self.rho ** abs(i - j)
        return sigma


class ConstantOffDiagCorrelationMatrix:
    def __init__(self, p):
        self.p = p
        self.value = 0.1

    def set_param(self, value):
        self.value = value
        return self

    def generate(self):
        sigma = np.eye(self.p)
        for i in range(self.p):
            for j in range(self.p):
                if i != j:
                    sigma[i, j] = self.value
        return sigma
