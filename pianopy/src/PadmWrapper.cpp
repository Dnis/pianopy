// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "../include/PadmWrapper.h"
#include <BssPadm.h>
#include <DataStandardization.h>
#include "../include/SolutionConversion.h"
#include <Solution.h>
#include <carma/carma.h>

using namespace piano::bss;

template<class StartParameterSearch, class PenaltySeries>
using InternalL1 = piano::bss::adm::Variant::ADMSkeleton<StartParameterSearch, PenaltySeries, piano::bss::adm::Variant::L1Optimizer<piano::cvbss::Lasso<piano::cvbss::lasso::solver::internal::CoordinateDescent>>>;
using InternalAdm = ADM<adm::StartParameterSearch<adm::penalization::search::picasso::CoordinateDescentSearch>, adm::PenaltySeries::DoublingPenalty, InternalL1>;

#ifdef PIANO_USE_PICASSO
template<class StartParameterSearch, class PenaltySeries>
using PICASSOL1 = piano::bss::adm::Variant::ADMSkeleton<StartParameterSearch, PenaltySeries, piano::bss::adm::Variant::L1Optimizer<piano::cvbss::Lasso<piano::cvbss::lasso::solver::picasso::CoordinateDescent>>>;
using PicassoAdm = ADM<adm::StartParameterSearch<adm::penalization::search::picasso::CoordinateDescentSearch>, adm::PenaltySeries::DoublingPenalty, PICASSOL1>;
#endif

#ifdef PIANO_USE_MLPACK
template<class StartParameterSearch, class PenaltySeries>
using MLPACKL1 = piano::bss::adm::Variant::ADMSkeleton<StartParameterSearch, PenaltySeries, piano::bss::adm::Variant::L1Optimizer<piano::cvbss::Lasso<piano::cvbss::lasso::solver::mlpack::LARS>>>;
using MlpackAdm = ADM<adm::StartParameterSearch<adm::penalization::search::mlpack::LARSSearch>, adm::PenaltySeries::DoublingPenalty, MLPACKL1>;
#endif

py::dict
pianopy::padmWrapper(py::array_t<double> &X, py::array_t<double> &y, int k, double ridgeParameter, int timeLimit,
                     bool intercept, bool normalise, bool mute, bool doStartParameterSearch, double startPenalization,
                     int numberOfThreads, double startPenalizationCorrection, std::string &lasso_solver) {
    arma::mat XA = carma::arr_to_mat<double>(X);
    arma::vec yA = carma::arr_to_col<double>(y);
    /*std::cout << "Cols: " << XA.n_cols << ", Rows: " << XA.n_rows << "\n";
    for (size_t i=0; i<XA.n_cols; ++i) {
        for (size_t j=0; j<XA.n_rows; ++j) {
            std::cout << XA.at(j, i) << "\n";
        }
    }*/
    piano::DataStandardization dataTransformation(XA, yA, ridgeParameter, normalise, intercept);

    piano::Solution sol;
    bool solved = false;

    if (lasso_solver == "picasso") {
#ifdef PIANO_USE_PICASSO
        PicassoAdm padm(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
        padm.setTimelimit(timeLimit).mute(mute).doStartParameterSearch(doStartParameterSearch).setStartPenalization(
                startPenalization).setNumberOfThreads(numberOfThreads).setStartParameterCorrection(
                startPenalizationCorrection);
        sol = padm.run(k);
        solved = true;
#else
        std::cerr << "PICASSO not build. Switching to internal solver.\n;";
#endif
    }

        if (lasso_solver == "mlpack") {
#ifdef PIANO_USE_MLPACK
            MlpackAdm padm(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
            padm.setTimelimit(timeLimit).mute(mute).doStartParameterSearch(doStartParameterSearch).setStartPenalization(
                    startPenalization).setNumberOfThreads(numberOfThreads).setStartParameterCorrection(
                    startPenalizationCorrection);
            sol = padm.run(k);
            solved = true;
#else
            std::cerr << "PICASSO not build. Switching to internal solver.\n;";
#endif
        }

        if ((lasso_solver == "internal") || (!solved)) {
            InternalAdm padm(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
            padm.setTimelimit(timeLimit).mute(mute).doStartParameterSearch(doStartParameterSearch).setStartPenalization(
                    startPenalization).setNumberOfThreads(numberOfThreads).setStartParameterCorrection(
                    startPenalizationCorrection);
            sol = padm.run(k);
            solved = true;
        }


    return SolutionToPyDict(dataTransformation.recoverSolution(sol));
}
