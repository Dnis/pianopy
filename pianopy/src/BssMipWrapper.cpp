// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "../include/BssMipWrapper.h"
#include <BssMip.h>
#include "../include/SolutionConversion.h"
#include <DataStandardization.h>
#include <pybind11/iostream.h>
#include <carma/carma.h>

using namespace piano::bss;

py::dict
pianopy::bssMipWrapper(py::array_t<double> &X, py::array_t<double> &y, int k, double ridgeParameter,
              const std::string &indicatorConstraintsHandling,
              const std::variant<py::dict, py::none> &warmstartSolution, int numberOfThreads, int timeLimit,
              bool intercept, bool normalise, bool muteSolver) {
    arma::mat XA = carma::arr_to_mat<double>(X);
    arma::vec yA = carma::arr_to_col<double>(y);
    piano::DataStandardization dataTransformation(XA, yA, ridgeParameter, normalise, intercept);
    if ((indicatorConstraintsHandling == "LogicalConstraints") || (indicatorConstraintsHandling == "Logical")) {
        Mip<NoWarmstart> bss(*(dataTransformation.getX()),
                    *(dataTransformation.getY()),
                    *(dataTransformation.getGamma()));
        bss.setTimeLimit(timeLimit).muteSolver(
                muteSolver).setNumberOfThreads(numberOfThreads);
        if (std::get_if<py::dict>(&warmstartSolution) != nullptr) {
            bss.addWarmstartSolution(PyDictToSolution(std::get<py::dict>(warmstartSolution)));
        }
        return SolutionToPyDict(dataTransformation.recoverSolution(bss.run(k)));
    }
    if ((indicatorConstraintsHandling == "BigMConstraints") || (indicatorConstraintsHandling == "BigM")) {
        Mip<NoWarmstart, mip::IndicatorConstraintsHandler::Cplex::BigMConstraints<>> bss(
                *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
        bss.setTimeLimit(timeLimit).muteSolver(
                muteSolver).setNumberOfThreads(numberOfThreads);
        if (std::get_if<py::dict>(&warmstartSolution) != nullptr) {
            bss.addWarmstartSolution(PyDictToSolution(std::get<py::dict>(warmstartSolution)));
        }
        return SolutionToPyDict(dataTransformation.recoverSolution(bss.run(k)));
    }
}