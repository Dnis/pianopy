// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "../include/MaxMinWrapper.h"
#include <DataStandardization.h>
#include "../include/SolutionConversion.h"
#include <MaxMin.h>
#include <Solution.h>
#include <carma/carma.h>

using namespace piano::bss;
using namespace piano;

py::dict
pianopy::maxMinWrapper(py::array_t<double> &X, py::array_t<double> &y, size_t k, double ridgeParameter, size_t timeLimit,
                       bool intercept, bool normalise, bool mute, bool extractEigenvalue, size_t numberOfThreads) {
    arma::mat XA = carma::arr_to_mat<double>(X);
    arma::vec yA = carma::arr_to_col<double>(y);
    piano::DataStandardization dataStandardization(XA, yA, ridgeParameter, normalise, intercept);

    Solution sol;
    if (extractEigenvalue) {
        MaxMin<> maxMin(*(dataStandardization.getX()), *(dataStandardization.getY()), *(dataStandardization.getGamma()));
        sol = maxMin.setNumberOfThreads(numberOfThreads).setTimeLimit(timeLimit).mute(mute).run(k);
    } else {
        MaxMin<> maxMin(*(dataStandardization.getX()), *(dataStandardization.getY()), *(dataStandardization.getGamma()), 0);
        sol = maxMin.setNumberOfThreads(numberOfThreads).setTimeLimit(timeLimit).mute(mute).run(k);
    }
    return SolutionToPyDict(dataStandardization.recoverSolution(sol));
}
