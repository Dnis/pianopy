// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <pybind11/pybind11.h>
#include "CvbssMipWrapper.h"
#include <LassoWrapper.h>
#include <PadmWrapper.h>
#include <BssMipWrapper.h>
#include <PadmHptWrapper.h>
#include <LassoHptWrapper.h>
#include <MaxMinWrapper.h>

namespace py = pybind11;
using namespace pybind11::literals;

PYBIND11_MODULE(_pianopy, mPianopy) {
    auto mCvbss = mPianopy.def_submodule("cvbss");
    auto mCvbssExact = mCvbss.def_submodule("exact");
    mCvbssExact.def("mip", &pianopy::cvbssMip, "X"_a, "y"_a, "ridge_parameter"_a = 0.0,
                    "indicator_constraints_handling"_a = "LogicalConstraints", "warmstart"_a = py::none(),
                    "number_of_threads"_a = 1, "number_of_partitions"_a = 10, "timelimit"_a = 120,
                    "intercept"_a = false,
                    "normalise"_a = true, "mute_solver"_a = true);

    auto mCvbssInexact = mCvbss.def_submodule("inexact");
    mCvbssInexact.def("lasso", &pianopy::lassoWrapper, "X"_a, "y"_a, "lambda"_a, "ridge_parameter"_a = 0.0,
                      "number_of_threads"_a = 1,
                      "intercept"_a = false, "normalise"_a = true);
    auto mCvbssInexactHpt = mCvbssInexact.def_submodule("hpt");
    mCvbssInexactHpt.def("lasso", &pianopy::lassoHptWrapper, "X"_a, "y"_a, "ridge_parameter"_a = 0.0,
                         "number_of_threads"_a = 1, "grid_size"_a = 100, "min_lambda"_a = 0, "max_lambda"_a = 5,
                         "intercept"_a = false, "normalise"_a = true);

    auto mBss = mPianopy.def_submodule("bss");
    auto mBssExact = mBss.def_submodule("exact");
    mBssExact.def("mip", &pianopy::bssMipWrapper, "X"_a, "y"_a, "k"_a,
                  py::arg_v("ridge_parameter", 0.0, "Ridge Parameter (default = 0.0)"),
                  py::arg_v("indicator_constraints_handling", "BigMConstraints",
                            "How indicator constraints are handled. Possible values are: BigMConstraints/BigM, LogicalConstraints/Logical (default = BigMConstraints)"),
                  py::arg_v("warmstart", py::none(), "Warmstart (default = None)"),
                  py::arg_v("number_of_threads", 1, "Number of threads (default = 1)"),
                  py::arg_v("timelimit", 120, "Timelimit (default = 1)"),
                  py::arg_v("intercept", false, "Whether or not to use intercept (default = False)"),
                  py::arg_v("normalise", true, "Whether or not to normalise the data (default = True)"),
                  py::arg_v("mute", false, "Whether or not to mute the solver (default = False)"));

    auto mBssInexact = mBss.def_submodule("inexact");
    mBssInexact.def("padm", &pianopy::padmWrapper, "X"_a, "y"_a, "k"_a,
                    py::arg_v("ridge_parameter", 0.0, "Ridge Parameter (default = 0.0)"),
                    py::arg_v("timelimit", 120, "Time limit in seconds (default = 120)"),
                    py::arg_v("intercept", false, "Whether or not to use intercept (default = False)"),
                    py::arg_v("normalise", true, "Whether or not to normalise the data (default = True)"),
                    py::arg_v("mute", false, "Whether or not to mute the solver (default = False)"),
                    py::arg_v("do_start_parameter_search", true,
                              "Whether or not to do a start penalization search (default = True)"),
                    py::arg_v("start_penalization", 1e-1, "Start penalization (default = 0.1)"),
                    py::arg_v("number_of_threads", 1, "Number of used threads (default = 1)."),
                    py::arg_v("start_penalization_correction", 0.8,
                              "Has only an effect if do_start_parameter_search is on. Reduces the found penalization by the given factor (default = 0.8)."),
                    py::arg_v("lasso_solver", "picasso",
                              "Choice of lasso solver. 'internal', 'mlpack' and 'picasso' are valid choices (default = 'mlpack')"));

    mBssInexact.def("max_min", &pianopy::maxMinWrapper, "X"_a, "y"_a, "k"_a,
                    py::arg_v("ridge_parameter", 0.0, "Ridge parameter (default = 0.0)"),
                    py::arg_v("timelimit", 120, "Time limit in seconds (default = 120)"),
                    py::arg_v("intercept", false, "Whether or not to use intercept (default = False)"),
                    py::arg_v("normalise", true, "Whether or not to normalise the data (default = True)"),
                    py::arg_v("mute", false, "Whether or not to mute the solver (default = False)"),
                    py::arg_v("extract_eigenvalue", true,
                              "Whether or not to extract the minimum eigenvalue in order to increase or generate a ridge parameter. If ridge_parameter is set to 0, this flag must be true (default = True)"),
                    py::arg_v("number_of_threads", 1, "Number of threads (default = 1)"));

    auto mBssInexactHpt = mBssInexact.def_submodule("hpt");
    mBssInexactHpt.def("padm", &pianopy::padmHptWrapper, "X"_a, "y"_a,
                       py::arg_v("ridge_parameter", 0.0, "Ridge parameter (default = 0.0)"),
                       py::arg_v("timelimit", 3600, "Time limit in seconds (default = 120)"),
                       py::arg_v("intercept", false, "Whether or not to use intercept (default = False)"),
                       py::arg_v("normalise", true, "Whether or not to normalise the data (default = True)"),
                       py::arg_v("mute", false, "Whether or not to mute the solver (default = False)"),
                       py::arg_v("number_of_threads", 1, "Number parallel grid evaluations."));
}

