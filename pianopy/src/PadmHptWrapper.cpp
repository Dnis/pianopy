// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "../include/PadmHptWrapper.h"
#include <BssPadm.h>
#include <DataStandardization.h>
#include "../include/SolutionConversion.h"
#include <carma/carma.h>

using namespace piano::bss;

py::dict pianopy::padmHptWrapper(py::array_t<double> &X, py::array_t<double> &y, double ridgeParameter, int timeLimit,
                                 bool intercept, bool normalise, bool mute, int numberOfThreads) {
    arma::mat XA = carma::arr_to_mat<double>(X);
    arma::vec yA = carma::arr_to_col<double>(y);
    std::cout << "Cols: " << XA.n_cols << ", Rows: " << XA.n_rows << std::endl;
    piano::DataStandardization dataTransformation(XA, yA, ridgeParameter, normalise, intercept);

    ADM<> padm(*(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
    padm.setTimelimit(timeLimit);
    auto padmHpt = padm.getHyperParameterTuner();
    padmHpt.mute(mute);
    padmHpt.setNumberOfThreads(numberOfThreads);

    auto sol = padmHpt.run();
    return SolutionToPyDict(dataTransformation.recoverSolution(sol));
}
