// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "../include/CvbssMipWrapper.h"
#include <CvbssMip.h>
#include "../include/SolutionConversion.h"
#include <DataStandardization.h>
#include <pybind11/iostream.h>
#include <carma/carma.h>

using namespace piano::cvbss;

py::dict pianopy::cvbssMip(py::array_t<double> &X, py::array_t<double> &y, double ridgeParameter,
                           const std::string &indicatorConstraintsHandling,
                           const std::variant<py::dict, py::none> &warmstartSolution, int numberOfThreads,
                           int numberOfPartitions, int timeLimit, bool intercept, bool normalise, bool muteSolver) {
    /*py::scoped_ostream_redirect stream(
            std::cout,                               // std::ostream&
            py::module::import("sys").attr("stdout") // Python output
    );*/

    arma::mat XA = carma::arr_to_mat<double>(X);
    arma::vec yA = carma::arr_to_col<double>(y);
    piano::DataStandardization dataTransformation(XA, yA, ridgeParameter, normalise, intercept);
    if ((indicatorConstraintsHandling == "LogicalConstraints") || (indicatorConstraintsHandling == "Logical")) {
        Mip<> cvbss(*(dataTransformation.getX()),
                                                   *(dataTransformation.getY()),
                                                   *(dataTransformation.getGamma()));
        cvbss.setNumberOfPartitions(numberOfPartitions).setTimeLimit(timeLimit).muteSolver(
                muteSolver).setNumberOfThreads(numberOfThreads);
        if (std::get_if<py::dict>(&warmstartSolution) != nullptr) {
            cvbss.addWarmstartSolution(PyDictToSolution(std::get<py::dict>(warmstartSolution)));
        }
        return SolutionToPyDict(dataTransformation.recoverSolution(cvbss.run()));
    }
    if ((indicatorConstraintsHandling == "BigMConstraints") || (indicatorConstraintsHandling == "BigM")) {
        Mip<NoWarmstart, mip::IndicatorConstraintsHandler::Cplex::BigMConstraints<>> cvbss(
                *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
        cvbss.setNumberOfPartitions(numberOfPartitions).setTimeLimit(timeLimit).muteSolver(
                muteSolver).setNumberOfThreads(numberOfThreads);
        if (std::get_if<py::dict>(&warmstartSolution) != nullptr) {
            cvbss.addWarmstartSolution(PyDictToSolution(std::get<py::dict>(warmstartSolution)));
        }
        return SolutionToPyDict(dataTransformation.recoverSolution(cvbss.run()));
    }
    if ((indicatorConstraintsHandling == "LogicalConstraintsWithBounds") ||
        (indicatorConstraintsHandling == "LogicalWithBounds")) {
        Mip<NoWarmstart, mip::IndicatorConstraintsHandler::Cplex::LogicalConstraintsWithBounds<>> cvbss(
                *(dataTransformation.getX()), *(dataTransformation.getY()), *(dataTransformation.getGamma()));
        cvbss.setNumberOfPartitions(numberOfPartitions).setTimeLimit(timeLimit).muteSolver(
                muteSolver).setNumberOfThreads(numberOfThreads);
        if (std::get_if<py::dict>(&warmstartSolution) != nullptr) {
            cvbss.addWarmstartSolution(PyDictToSolution(std::get<py::dict>(warmstartSolution)));
        }
        return SolutionToPyDict(dataTransformation.recoverSolution(cvbss.run()));
    }
    throw std::invalid_argument(indicatorConstraintsHandling + " is not a valid argument.");
}
