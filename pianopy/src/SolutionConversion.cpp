// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "../include/SolutionConversion.h"
#include <carma/carma.h>
#include <pybind11/stl.h>

py::dict pianopy::SolutionToPyDict(const piano::Solution &sol) {
    arma::vec coefsCopy(sol.coefficients);
    arma::vec gammaCopy(sol.gamma);
    py::dict d;
    d["objective_value"] = sol.objectiveValue;
    d["rss"] = sol.rss;
    d["subset"] = sol.subset;
    d["coefficients"] = carma::col_to_arr(std::move(coefsCopy));
    d["n"] = sol.n;
    d["p"] = sol.p;
    d["subset_size"] = sol.subsetSize;
    d["estimated_prediction_error"] = sol.estimatedPredictionError;
    d["gamma"] = carma::col_to_arr(std::move(gammaCopy));
    d["provable_optimal"] = sol.provableOptimal;
    d["intercept"] = sol.intercept;
    d["complete_runtime"] = sol.completeRuntimeInSeconds;
    d["computations_runtime"] = sol.computationsRuntimeInSeconds;
    d["mse"] = sol.mse();
    d["full_coefficients"] = carma::col_to_arr(sol.fullCoefficients());
    d["ridge_parameter"] = sol.ridgeParameter();
    d["indicator_vector"] = carma::col_to_arr(arma::conv_to<arma::vec>::from(sol.indicatorVector()));
    d["gap"] = sol.gap;
    d["sanity_check"] = sol.sanityCheck;
    d["null_score"] = sol.nullScore;
    d["timelimit_reached"] = sol.reachedTimelimit;
    d["max_iteration_count_reached"] = sol.reachedIterationCount;
    return d;
}

piano::Solution pianopy::PyDictToSolution(const py::dict &d) {
    piano::Solution sol;
    sol.objectiveValue = d["objective_value"].cast<double>();
    sol.rss = d["rss"].cast<double>();
    sol.subset = d["subset"].cast<std::vector<int>>();
    sol.coefficients = d["coefficients"].cast<arma::vec>();
    sol.n = d["n"].cast<int>();
    sol.p = d["p"].cast<int>();
    sol.subsetSize = d["subset_size"].cast<int>();
    sol.estimatedPredictionError = d["estimated_prediction_error"].cast<double>();
    sol.gamma = d["gamma"].cast<arma::vec>();
    sol.provableOptimal = d["provable_optimal"].cast<bool>();
    sol.intercept = d["intercept"].cast<double>();
    sol.completeRuntimeInSeconds = d["complete_runtime"].cast<double>();
    sol.computationsRuntimeInSeconds = d["computations_runtime"].cast<double>();
    sol.gap = d["gap"].cast<double>();
    sol.sanityCheck = d["sanity_check"].cast<bool>();
    sol.nullScore = d["null_score"].cast<double>();
    sol.reachedTimelimit = d["timelimit_reached"].cast<bool>();
    sol.reachedIterationCount = d["max_iteration_count_reached"].cast<bool>();
    return sol;
}
